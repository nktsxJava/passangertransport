
import Entities.User;
import Enums.UserTypes;
import Repositories.UserRepository;
import Services.Classes.LoginServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import utils.UserSession;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class LoginServiceTest {

    private User user;
    private User returnedUser;
    private User wrongNameUser;
    private User wrongPassUser;

    private static final String USERNAME = "Admin";
    private static final String HASHPASSWORD = "$2a$12$A5PZ5dEapSHNOeG25jo1fetW/UZZWCgzixCqLwEPY0FdiRIEbGfmq";
    private static final String PASSWORD = "12345";

    private static final String WRONG_USERNAME = "username";
    private static final String WRONG_PASSWORD = "9999";

    @InjectMocks
    private LoginServiceImpl loginService;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        user = new User();
        user.setName(USERNAME);
        user.setPassword(PASSWORD);
        user.setType(UserTypes.toInteger(UserTypes.Admin));

        returnedUser = new User();
        returnedUser.setName(USERNAME);
        returnedUser.setPassword(HASHPASSWORD);
        returnedUser.setType(UserTypes.toInteger(UserTypes.Admin));

        wrongNameUser = new User();
        wrongNameUser.setName(WRONG_USERNAME);
        wrongNameUser.setPassword(PASSWORD);
        wrongNameUser.setType(UserTypes.toInteger(UserTypes.Admin));

        wrongPassUser = new User();
        wrongPassUser.setName(USERNAME);
        wrongPassUser.setPassword(WRONG_PASSWORD);
        wrongPassUser.setType(UserTypes.toInteger(UserTypes.Admin));

        when(userRepository.findByUserName(USERNAME)).thenReturn(returnedUser);
        UserSession.clearUserSession();
    }

    @Test
    public void testLogIn() {
        assertTrue(loginService.verifyLogin(user));
    }

    @Test
    public void testLogInWithWrongUsername() {
        assertFalse(loginService.verifyLogin(wrongNameUser));
    }

    @Test
    public void testLogInWithWrongPassword() {
        assertFalse(loginService.verifyLogin(wrongPassUser));
    }

    @Test
    public void testLogOut() {
        UserSession.createUserSession(new User());
        loginService.logOut();
        assertNull(UserSession.getUserSession());
    }
}
