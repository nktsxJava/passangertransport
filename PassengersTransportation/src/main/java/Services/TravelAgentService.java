package Services;

import Entities.TicketApplicationRequest;
import Entities.Travel;

public interface TravelAgentService {
    public void createTravel(Travel travel);
    public void acceptDistributorTickets(TicketApplicationRequest ticketApplicationRequest);
}

