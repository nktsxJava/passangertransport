package Services;

import Entities.User;

public interface LoginService {
    public boolean verifyLogin( User user );
    public void logOut();
    public void saveLoggedUser( User user );
    public void saveLoggedOutUser(User user);
}
