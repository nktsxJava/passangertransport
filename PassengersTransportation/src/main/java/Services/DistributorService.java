package Services;

import Entities.Cashier;
import Entities.TicketApplicationRequest;
import Entities.Travel;

public interface DistributorService {
    public Cashier hireCashier(Cashier hiredCashier);
    public void makeTicketApplicationRequest(TicketApplicationRequest ticketApplicationRequest);
}
