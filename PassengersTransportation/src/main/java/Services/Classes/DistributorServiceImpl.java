package Services.Classes;

import Entities.Cashier;
import Entities.Distributor;
import Entities.TicketApplicationRequest;
import Entities.Travel;
import Repositories.BaseTableRepository;
import Repositories.Classes.CashierRepositoryImpl;
import Repositories.Classes.TicketApplicationRequestRepositoryImpl;
import Services.DistributorService;
import utils.UserSession;

public class DistributorServiceImpl implements DistributorService {
    @Override
    public Cashier hireCashier(Cashier hiredCashier) {
        BaseTableRepository<Cashier> cashierRepository = new CashierRepositoryImpl();

        Distributor loggedDistributor = UserSession.getUserSession().getLoggedUser().getRole();
        hiredCashier.setDistributor(loggedDistributor);

        return cashierRepository.update(hiredCashier);
    }

    @Override
    public void makeTicketApplicationRequest(TicketApplicationRequest ticketApplicationRequest) {

        Distributor loggedDistributor = UserSession.getUserSession().getLoggedUser().getRole();
        ticketApplicationRequest.setDistributor(loggedDistributor);
        ticketApplicationRequest.setTravelAgent(loggedDistributor.getTravelAgent());

        BaseTableRepository<TicketApplicationRequest> ticketApplicationRequestRepository = new TicketApplicationRequestRepositoryImpl();
        ticketApplicationRequestRepository.create(ticketApplicationRequest);
    }
}
