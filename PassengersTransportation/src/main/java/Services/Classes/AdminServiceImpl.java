package Services.Classes;

import Entities.*;
import Repositories.*;
import Repositories.Classes.*;
import Services.AdminService;

import java.util.List;

public class AdminServiceImpl implements AdminService {

    @Override
    public void createTravelCompany(TravelCompany travelCompany) {
        BaseTableRepository travelCompanyRepository = new TravelCompanyRepositoryImpl();
        travelCompanyRepository.create(travelCompany);
    }

    @Override
    public void createTravelAgent(User travelAgentUser, TravelCompany travelCompany) {
        BaseTableRepository userRepository = new UserRepositoryImpl();
        userRepository.create(travelAgentUser);

        TravelAgent travelAgent = new TravelAgent();
        travelAgent.setUser(travelAgentUser);
        travelAgent.setTravelCompany(travelCompany);

        TravelAgentRepository travelAgentRepository = new TravelAgentRepositoryImpl();
        travelAgentRepository.create(travelAgent);
    }

    @Override
    public void createDistributor(User distributorUser) {
        UserRepository userRepository = new UserRepositoryImpl();
        userRepository.create(distributorUser);

        Distributor distributor = new Distributor();
        distributor.setUser(distributorUser);

        TravelAgent travelAgent = new TravelAgent(0, new User(), new TravelCompany());
        distributor.setTravelAgent(travelAgent); // слагаме дефолтно, да не е назначен от никой организатор

        DistributorRepository distributorRepository = new DistributorRepositoryImpl();
        distributorRepository.create(distributor);
    }

    @Override
    public void createCashier(User cashierUser) {
        UserRepository userRepository = new UserRepositoryImpl();
        userRepository.create(cashierUser);

        Cashier cashier = new Cashier();
        cashier.setUser(cashierUser);

        Distributor distributor = new Distributor(0, new User(), new TravelAgent());
        cashier.setDistributor(distributor); // слагаме дефолтно, да не е назанчен от никой дистрибутор

        BaseTableRepository cashierRepository = new CashierRepositoryImpl();
        cashierRepository.create(cashier);
    }

    @Override
    public List<TravelCompany> loadAllTravelCompanies() {
        TravelCompaniesRepository travelCompaniesRepository = new TravelCompanyRepositoryImpl();
        return travelCompaniesRepository.loadAllTravelCompanies();
    }

    @Override
    public List<User> loadAllUsers() {
        UserRepository  userRepository = new UserRepositoryImpl();
        return userRepository.loadAllUsers();
    }
}
