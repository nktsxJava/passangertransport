package Services.Classes;

import Entities.Customer;
import Entities.Order;
import Entities.Ticket;
import Repositories.BaseTableRepository;
import Repositories.Classes.CustomerRepositoryImpl;
import Repositories.Classes.OrderRepositoryImpl;
import Repositories.CustomerRepository;
import Services.CashierService;
import utils.UserSession;

public class CashierServiceImpl implements CashierService {
    @Override
    public void saleTicket(Ticket soldTicket, Customer customer) {

        CustomerRepository customerRepository = new CustomerRepositoryImpl();
        if(!customerRepository.findCustomer(customer.getName(), customer.getLastName(), customer.getUcn()))
        {
            customerRepository.create(customer);
        }

        Order order = new Order();
        order.setCustomer(customer);
        order.setTicket(soldTicket);
        order.setCashier(UserSession.getUserSession().getLoggedUser().getRole());

        BaseTableRepository<Order> orderRepository = new OrderRepositoryImpl();
        orderRepository.create(order);
    }
}
