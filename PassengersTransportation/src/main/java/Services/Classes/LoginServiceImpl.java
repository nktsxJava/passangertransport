package Services.Classes;

import Entities.LoggedHistory;
import Entities.User;
import Enums.LoginStatuses;
import Repositories.LoggedHistoryRepository;
import Repositories.Classes.LoggedHistoryRepositoryImpl;
import Repositories.Classes.UserRepositoryImpl;
import Repositories.UserRepository;
import Services.LoginService;
import org.mindrot.jbcrypt.BCrypt;
import utils.UserSession;
import java.util.Date;
import java.sql.Timestamp;

public class LoginServiceImpl implements LoginService {

    private LoggedHistoryRepository loggedHistoryRepository;

    public LoginServiceImpl() {
        this.loggedHistoryRepository = new LoggedHistoryRepositoryImpl();
    }

    public boolean verifyLogin(User user) {

        UserRepository userRepository = new UserRepositoryImpl();
        if( userRepository.findByUserName(user.getName()) == null)
            return false;

        User returnedUser = userRepository.findByUserName(user.getName());
        String hashedPass = returnedUser.getPassword();

        boolean isPassValid = BCrypt.checkpw(user.getPassword(), hashedPass);
        if( !isPassValid )
            return false;

        UserSession.createUserSession(returnedUser);
        saveLoggedUser(returnedUser);

        user.setId(returnedUser.getId());
        user.setCountry(returnedUser.getCountry());
        user.setCity(returnedUser.getCity());
        user.setAddress(returnedUser.getAddress());
        user.setType(returnedUser.getType());
        user.setEmail(returnedUser.getEmail());

        return true;
    }

    public void logOut() {
        User loggedUser = UserSession.getUserSession().getLoggedUser();
        saveLoggedOutUser(loggedUser);
        UserSession.clearUserSession();
    }

    public void saveLoggedUser(User user) {
        //Създаваме нов запис за логгнат портебител
        LoggedHistory loggedHistory = new LoggedHistory();
        loggedHistory.setUser(user);

        //С текущо време
        Date currDate = new Date();
        Timestamp currTimeStamp = new Timestamp(currDate.getTime());
        loggedHistory.setLogInTime(currTimeStamp);

        //и статус на LOGGED
        loggedHistory.setStatus( LoginStatuses.LOGGED );

        //записваме обекта в таблицата в базата
        loggedHistoryRepository.saveHistory( loggedHistory );
    }

    public void saveLoggedOutUser(User user) {
        LoggedHistory loggedHistory = new LoggedHistory();
        loggedHistory.setUser(user);

        //С текущо време задаваме времето на излизане от потребителя
        Date currDate = new Date();
        Timestamp currTimeStamp = new Timestamp(currDate.getTime());
        loggedHistory.setLogOutTime(currTimeStamp);

        //и статус на OFFLINE
        loggedHistory.setStatus( LoginStatuses.OFFLINE );

        loggedHistoryRepository.saveHistory( loggedHistory );
    }
}