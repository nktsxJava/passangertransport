package Services.Classes;

import Entities.DistributorTravelTickets;
import Entities.TicketApplicationRequest;
import Entities.Travel;
import Enums.TicketApplicationRequestStatus;
import Repositories.BaseTableRepository;
import Repositories.Classes.DistributorTravelTicketsRepositoryImpl;
import Repositories.Classes.TravelRepositoryImpl;
import Repositories.DistributorTravelTicketsRepository;
import Services.TravelAgentService;
import utils.UserSession;

import java.util.List;

public class TravelAgentServiceImpl implements TravelAgentService {
    @Override
    public void createTravel(Travel travel) {

        travel.setTravelAgent(UserSession.getUserSession().getLoggedUser().getRole());
        BaseTableRepository<Travel> travelRepository = new TravelRepositoryImpl();
        travelRepository.create(travel);
    }

    @Override
    public void acceptDistributorTickets(TicketApplicationRequest ticketApplicationRequest) {
        ticketApplicationRequest.setStatus(TicketApplicationRequestStatus.toInteger(TicketApplicationRequestStatus.ACCEPTED));

        DistributorTravelTickets distributorTravelTickets = new DistributorTravelTickets();
        distributorTravelTickets.setDistributor(ticketApplicationRequest.getDistributor());
        distributorTravelTickets.setTravel(ticketApplicationRequest.getTravel());
        distributorTravelTickets.setTicketsCount(ticketApplicationRequest.getTicketsCount());


        DistributorTravelTicketsRepository distributorTravelTicketsRepository = new DistributorTravelTicketsRepositoryImpl();

        List<DistributorTravelTickets> results = distributorTravelTicketsRepository.findTravel(ticketApplicationRequest.getTravel());
        if(results.isEmpty()) {
            distributorTravelTicketsRepository.create(distributorTravelTickets);
        }
        else {
            DistributorTravelTickets result = results.get(0);
            distributorTravelTickets.setTicketsCount(result.getTicketsCount() + distributorTravelTickets.getTicketsCount());

            distributorTravelTicketsRepository.update(distributorTravelTickets);
        }
    }
}
