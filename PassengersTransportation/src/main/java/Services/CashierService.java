package Services;

import Entities.Customer;
import Entities.Ticket;

public interface CashierService {
    public void saleTicket(Ticket soldTicket, Customer customer);
}
