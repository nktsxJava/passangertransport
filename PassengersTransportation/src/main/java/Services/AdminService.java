package Services;

import Entities.*;

import java.util.List;

public interface AdminService {
    public void createTravelCompany(TravelCompany travelCompany);
    public void createTravelAgent(User TravelAgent, TravelCompany travelCompany);
    public void createDistributor(User Distributor);
    public void createCashier(User Cashier);
    public List<TravelCompany> loadAllTravelCompanies();
    public List<User> loadAllUsers();
}
