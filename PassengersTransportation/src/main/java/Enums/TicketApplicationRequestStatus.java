package Enums;

public enum TicketApplicationRequestStatus {
    ACCEPTED,
    DENIEND;

    public static TicketApplicationRequestStatus fromInteger(Integer x) {
        switch(x) {
            case 0:
                return ACCEPTED;
            case 1:
                return DENIEND;
        }
        return null;
    }

    public static Integer toInteger(TicketApplicationRequestStatus x) {
        switch(x) {
            case ACCEPTED:
                return 0;
            case DENIEND:
                return 1;
        }
        return -1;
    }
}
