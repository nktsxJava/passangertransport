package Enums;

public enum UserTypes{
    Admin,
    TravelAgent,
    Distributor,
    Cashier;

    public static UserTypes fromInteger(Integer x) {
        switch(x) {
            case 0:
                return Admin;
            case 1:
                return TravelAgent;
            case 2:
                return Distributor;
            case 3:
                return Cashier;
        }
        return null;
    }

    public static Integer toInteger(UserTypes x) {
        switch(x) {
            case Admin:
                return 0;
            case TravelAgent:
                return 1;
            case Distributor:
                return 2;
            case Cashier:
                return 3;
        }
        return -1;
    }
}
