package Enums;

public enum TransportTypes {
    Bus,
    Plane;

    public static TransportTypes fromInteger(Integer x) {
        switch(x) {
            case 0:
                return Bus;
            case 1:
                return Plane;
        }
        return null;
    }

    public static Integer toInteger(TransportTypes x) {
        switch(x) {
            case Bus:
                return 0;
            case Plane:
                return 1;
        }
        return -1;
    }
}
