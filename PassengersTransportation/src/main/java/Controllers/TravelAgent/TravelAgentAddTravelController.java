package Controllers.TravelAgent;

import Entities.Travel;
import Entities.TravelAgent;
import Enums.TransportTypes;
import Services.Classes.TravelAgentServiceImpl;
import Services.TravelAgentService;
import com.jfoenix.controls.JFXDatePicker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class TravelAgentAddTravelController implements Initializable {

    private TravelAgentLandingController parentController = new TravelAgentLandingController();

    @FXML
    private ComboBox<TransportTypes> comboTransportType;

    @FXML
    private TextField destinationField;

    @FXML
    private TextField seatsField;

    @FXML
    private JFXDatePicker departureField;

    @FXML
    private JFXDatePicker arrivalField;

    @FXML
    private Button btnCreate;

    @FXML
    private Button btnCancel;

    @FXML
    private Text invalidDestinationMessage;

    @FXML
    private Text invalidTranportTypeMessage;

    @FXML
    private Text invalidDepartureMessage;

    @FXML
    private Text invalidSeatsMessage;

    @FXML
    private Text invalidArrivalMessage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clearMessages();

        btnCancel.setOnAction(this::onCancel);
        btnCreate.setOnAction(this::onCreate);
    }

    public void setParentController(TravelAgentLandingController parentController) {
        this.parentController = parentController;
    }

    @FXML
    void onCreate(ActionEvent event) {

        clearMessages();
        if(!validateData())
            return;

        Travel travel = exchangeControlsToData();

        TravelAgentService travelAgentService = new TravelAgentServiceImpl();
        travelAgentService.createTravel(travel);
    }

    @FXML
    void onCancel(ActionEvent event) {
        parentController.clearLayOutField();
    }

    boolean validateData() {

        boolean hasInvalidMessage = false;

        if(destinationField.getText().isEmpty()) {
            invalidDestinationMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(comboTransportType.getSelectionModel().getSelectedIndex() < 0) {
            invalidTranportTypeMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(seatsField.getText().isEmpty()) {
            invalidSeatsMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        Integer validateSeatsCount = -1;
        try {
            validateSeatsCount = Integer.parseInt(seatsField.getText());
        } catch (NumberFormatException e) {
            validateSeatsCount = -1;
        }

        if(validateSeatsCount < 0)
        {
            invalidSeatsMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(hasInvalidMessage)
            return false;

        return true;
    }

    void clearMessages() {
        invalidDestinationMessage.setVisible(false);
        invalidTranportTypeMessage.setVisible(false);
        invalidSeatsMessage.setVisible(false);
        invalidDepartureMessage.setVisible(false);
        invalidArrivalMessage.setVisible(false);
    }

    Travel exchangeControlsToData() {

        Travel travel = new Travel();
        travel.setDestination(destinationField.getText());

        TransportTypes transportTypes = (TransportTypes)comboTransportType.getSelectionModel().getSelectedItem();
        travel.setTransportType(TransportTypes.toInteger(transportTypes));

        travel.setSeatsCount(Integer.parseInt(seatsField.getText()));
        travel.setArrival(arrivalField.getValue());
        travel.setDeparture(departureField.getValue());

        return travel;
    }
}
