package Controllers.TravelAgent;

import Controllers.LoginController;
import Repositories.Classes.CashierRepositoryImpl;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TravelAgentLandingController implements Initializable {

    private static final Logger logger = LogManager.getLogger(CashierRepositoryImpl.class);

    private Stage stage;

    @FXML
    private Button btnTravel;

    @FXML
    private Button btnAcceptTicketApplication;

    @FXML
    private Button btnListDistributors;

    @FXML
    private Button btnEditProfile;

    @FXML
    private Button btnLogOut;

    @FXML
    private AnchorPane layOutField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnTravel.setOnAction(this::addTravel);
        //btnAcceptTicketApplication.setOnAction(this::processTickeApplication);
        //btnListDistributors.setOnAction(this::listDistributors);
        //btnEditProfile.setOnAction(this::editProfile);
        btnLogOut.setOnAction(this::LogOut);
    }

    public void clearLayOutField() {
        layOutField.getChildren().clear();
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    void LogOut(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/Admin/AdminLanding.fxml"));
        Parent root = null;

        try {
            root = loader.load();
        } catch (IOException e) {
            logger.error("{}", e);
        }

        stage.setTitle("Loginscreen");
        stage.setScene(new Scene(root));

        LoginController controller = loader.getController();
        controller.setStage(stage);

        stage.show();
    }

    @FXML
    void addTravel(ActionEvent event) {
        Node node = null;
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/TravelAgent/AgentAddTravel.fxml"));

        try {
            node = (Node)loader.load();
            TravelAgentAddTravelController controller = loader.getController();
            controller.setParentController(this);

        } catch (IOException e) {
            logger.error("{}", e);
        }

        layOutField.getChildren().setAll(node);
    }

/*    @FXML
    void processTickeApplication(ActionEvent event) {
        Node node = null;
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/TravelAgent/AgentTicketApplication.fxml"));

        try {
            node = (Node)loader.load();
            AdminAddDistributor controller = loader.getController();
            controller.setParentController(this);

        } catch (IOException e) {
            logger.error("{}", e);
        }

        layOutField.getChildren().setAll(node);
    }*/

    /*@FXML
    void listDistributors(ActionEvent event) {
        Node node = null;
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/ListAllUsers.fxml"));

        try {
            node = (Node)loader.load();
            AdminAddTravelAgent controller = loader.getController();
            controller.setParentController(this);

        } catch (IOException e) {
            logger.error("{}", e);
        }

        layOutField.getChildren().setAll(node);
    }

    @FXML
    void editProfile(ActionEvent event) {

        Node node = null;
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/EditProfile.fxml"));

        try {
            node = (Node)loader.load();
            AdminAddTravelCompany controller = loader.getController();
            controller.setParentController(this);

        } catch (IOException e) {
            logger.error("{}", e);
        }

        layOutField.getChildren().setAll(node);
    }*/
}
