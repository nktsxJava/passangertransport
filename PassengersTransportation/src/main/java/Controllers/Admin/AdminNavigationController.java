package Controllers.Admin;

import Controllers.ListAllUsersController;
import Controllers.LoginController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AdminNavigationController implements Initializable {

    private Stage stage;

    @FXML
    private Button addTravelCompanyButton;

    @FXML
    private Button addTravelAgent;

    @FXML
    private Button addDistributorButton;

    @FXML
    private Button addCashierButton;

    @FXML
    private Button allUsersButton;

    @FXML
    private Button logOutButton;

    @FXML
    private AnchorPane layoutField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addTravelCompanyButton.setOnAction(this::createTravelCompany);
        addCashierButton.setOnAction(this::createCashier);
        addDistributorButton.setOnAction(this::createDistributor);
        addTravelAgent.setOnAction(this::createTravelAgent);
        allUsersButton.setOnAction(this::listAllUsers);
        logOutButton.setOnAction(this::LogOut);
    }

    public void clearLayOutField() {
        layoutField.getChildren().clear();
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    void LogOut(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/Admin/AdminLanding.fxml"));
        Parent root = null;

        try {
            root = loader.load();
        } catch (IOException e) {

        }

        stage.setTitle("Loginscreen");
        stage.setScene(new Scene(root));

        LoginController controller = loader.getController();
        controller.setStage(stage);

        stage.show();
    }

    @FXML
    void createCashier(ActionEvent event) {
        Node node = null;
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/Admin/AdminAddCashier.fxml"));

        try {
            node = (Node)loader.load();
            AdminAddCashier controller = loader.getController();
            controller.setParentController(this);

        } catch (IOException e) {

        }

        layoutField.getChildren().setAll(node);
    }

    @FXML
    void createDistributor(ActionEvent event) {
        Node node = null;
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/Admin/AdminAddDistributor.fxml"));

        try {
            node = (Node)loader.load();
            AdminAddDistributor controller = loader.getController();
            controller.setParentController(this);

        } catch (IOException e) {

        }

        layoutField.getChildren().setAll(node);
    }

    @FXML
    void createTravelAgent(ActionEvent event) {
        Node node = null;
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/Admin/AdminAddTravelAgent.fxml"));

        try {
            node = (Node)loader.load();
            AdminAddTravelAgent controller = loader.getController();
            controller.setParentController(this);

        } catch (IOException e) {

        }

        layoutField.getChildren().setAll(node);
    }

    @FXML
    void createTravelCompany(ActionEvent event) {

        Node node = null;
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/Admin/AdminAddTravelCompany.fxml"));

        try {
            node = (Node)loader.load();
            AdminAddTravelCompany controller = loader.getController();
            controller.setParentController(this);

        } catch (IOException e) {

        }

        layoutField.getChildren().setAll(node);

    }

    @FXML
    void listAllUsers(ActionEvent event) {
        Node node = null;
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/ListAllUsers.fxml"));

        try {
            node = (Node)loader.load();
            ListAllUsersController controller = loader.getController();
            controller.setParentAdminController(this);

        } catch (IOException e) {

        }

        layoutField.getChildren().setAll(node);
    }

}