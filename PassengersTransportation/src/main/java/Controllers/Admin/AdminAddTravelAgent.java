package Controllers.Admin;

import Entities.TravelCompany;
import Entities.User;
import Enums.UserTypes;
import Services.AdminService;
import Services.Classes.AdminServiceImpl;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import javax.swing.*;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class AdminAddTravelAgent implements Initializable {

    private AdminNavigationController parentController = new AdminNavigationController();

    private AdminService adminService = new AdminServiceImpl();

    @FXML
    private Label titleLabel;

    @FXML
    private TextField usernameField;

    @FXML
    private Text invalidUsernameMessage;

    @FXML
    private TextField emailField;

    @FXML
    private Text invalidEmailMessage;

    @FXML
    private TextField passwordField;

    @FXML
    private Text invalidPasswordMessage;

    @FXML
    private TextField addressField;

    @FXML
    private Text invalidAddressMessage;

    @FXML
    private TextField cityField;

    @FXML
    private Text invalidCityMessage;

    @FXML
    private TextField countryField;

    @FXML
    private Text invalidCountryField;

    @FXML
    private ComboBox<TravelCompany> travelCompanyField;

    @FXML
    private Text invalidTravelCompanyMessage;

    @FXML
    private Button buttonCancel;

    @FXML
    private Button buttonCreate;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clearMessages();

        List<TravelCompany> travelCompanyList = adminService.loadAllTravelCompanies();
        for(TravelCompany t : travelCompanyList)
            travelCompanyField.getItems().add(t);

        buttonCancel.setOnAction(this::onCancel);
        buttonCreate.setOnAction(this::onCreate);
    }


    public void setParentController(AdminNavigationController parentController) {
        this.parentController = parentController;
    }

    @FXML
    void onCreate(ActionEvent event) {

        clearMessages();
        if(!validateData())
            return;

        User travelAgentUser = exchangeControlsToData();

        TravelCompany travelCompany = (TravelCompany)travelCompanyField.getSelectionModel().getSelectedItem();

        adminService.createTravelAgent(travelAgentUser, travelCompany);
    }

    @FXML
    void onCancel(ActionEvent event) {
        parentController.clearLayOutField();
    }

    boolean validateData() {

        boolean hasInvalidMessage = false;

        if(usernameField.getText().isEmpty()) {
            invalidUsernameMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(passwordField.getText().isEmpty()) {
            invalidPasswordMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(emailField.getText().isEmpty()) {
            invalidEmailMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(addressField.getText().isEmpty()) {
            invalidAddressMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(cityField.getText().isEmpty()) {
            invalidCityMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(countryField.getText().isEmpty()) {
            invalidCountryField.setVisible(true);
            hasInvalidMessage = true;
        }

        if(travelCompanyField.getSelectionModel().getSelectedIndex() < 0) {
            invalidTravelCompanyMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(hasInvalidMessage)
            return false;

        return true;
    }

    void clearMessages() {
        invalidAddressMessage.setVisible(false);
        invalidCityMessage.setVisible(false);
        invalidCountryField.setVisible(false);
        invalidEmailMessage.setVisible(false);
        invalidPasswordMessage.setVisible(false);
        invalidTravelCompanyMessage.setVisible(false);
        invalidUsernameMessage.setVisible(false);
    }

    User exchangeControlsToData() {
        User travelAgentUser = new User();
        travelAgentUser.setName(usernameField.getText());
        travelAgentUser.setPassword(passwordField.getText());
        travelAgentUser.setEmail(emailField.getText());
        travelAgentUser.setType(UserTypes.toInteger(UserTypes.TravelAgent));
        travelAgentUser.setAddress(addressField.getText());
        travelAgentUser.setCity(cityField.getText());
        travelAgentUser.setCountry(countryField.getText());

        return travelAgentUser;
    }

}
