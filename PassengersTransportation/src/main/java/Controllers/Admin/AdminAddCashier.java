package Controllers.Admin;

import Entities.User;
import Enums.UserTypes;
import Services.AdminService;
import Services.Classes.AdminServiceImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class AdminAddCashier implements Initializable {

    private AdminNavigationController parentController = new AdminNavigationController();

    @FXML
    private Label titleLabel;

    @FXML
    private JFXTextField usernameField;

    @FXML
    private Text invalidUsernameMessage;

    @FXML
    private JFXTextField emailField;

    @FXML
    private Text invalidEmailMessage;

    @FXML
    private JFXTextField passwordField;

    @FXML
    private Text invalidPasswordMessage;

    @FXML
    private JFXTextField addressField;

    @FXML
    private Text invalidAddressMessage;

    @FXML
    private JFXTextField cityField;

    @FXML
    private Text invalidCityMessage;

    @FXML
    private JFXTextField countryField;

    @FXML
    private Text invalidCountryField;

    @FXML
    private JFXButton buttonCreate;

    @FXML
    private JFXButton buttonCancel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clearMessages();

        buttonCancel.setOnAction(this::onCancel);
        buttonCreate.setOnAction(this::onCreate);
    }

    public void setParentController(AdminNavigationController parentController) {
        this.parentController = parentController;
    }

    @FXML
    void onCreate(ActionEvent event) {

        clearMessages();
        if(!validateData())
            return;

        User cashier = exchangeControlsToData();

        AdminService adminService = new AdminServiceImpl();
        adminService.createCashier(cashier);
    }

    @FXML
    void onCancel(ActionEvent event) {
        parentController.clearLayOutField();
    }

    boolean validateData() {

        boolean hasInvalidMessage = false;

        if(usernameField.getText().isEmpty()) {
            invalidUsernameMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(passwordField.getText().isEmpty()) {
            invalidPasswordMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(emailField.getText().isEmpty()) {
            invalidEmailMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(addressField.getText().isEmpty()) {
            invalidAddressMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(cityField.getText().isEmpty()) {
            invalidCityMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(countryField.getText().isEmpty()) {
            invalidCountryField.setVisible(true);
            hasInvalidMessage = true;
        }

        if(hasInvalidMessage)
            return false;

        return true;
    }

    void clearMessages() {
        invalidAddressMessage.setVisible(false);
        invalidCityMessage.setVisible(false);
        invalidCountryField.setVisible(false);
        invalidEmailMessage.setVisible(false);
        invalidPasswordMessage.setVisible(false);
        invalidUsernameMessage.setVisible(false);
    }

    User exchangeControlsToData() {
        User cashierUser = new User();
        cashierUser.setName(usernameField.getText());
        cashierUser.setPassword(passwordField.getText());
        cashierUser.setEmail(emailField.getText());
        cashierUser.setAddress(addressField.getText());
        cashierUser.setCity(cityField.getText());
        cashierUser.setCountry(countryField.getText());
        cashierUser.setType(UserTypes.toInteger(UserTypes.Cashier));

        return cashierUser;
    }
}
