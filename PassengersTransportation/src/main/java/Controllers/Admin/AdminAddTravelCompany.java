package Controllers.Admin;

import Entities.TravelCompany;
import Services.AdminService;
import Services.Classes.AdminServiceImpl;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

public class AdminAddTravelCompany implements Initializable {

    private AdminNavigationController parentController = new AdminNavigationController();

    @FXML
    private Label titleLabel;

    @FXML
    private TextField nameField;

    @FXML
    private Text invalidNameMessage;

    @FXML
    private TextField addressField;

    @FXML
    private Text invalidAddressMessage;

    @FXML
    private TextField cityField;

    @FXML
    private Text invalidCityMessage;

    @FXML
    private TextField countryField;

    @FXML
    private Text invalidCountryMessage;

    @FXML
    private Button buttonCreate;

    @FXML
    private Button buttonCancel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        clearMessages();

        buttonCancel.setOnAction(this::onCancel);
        buttonCreate.setOnAction(this::onCreate);
    }

    public void setParentController(AdminNavigationController parentController) {
        this.parentController = parentController;
    }

    @FXML
    void onCreate(ActionEvent event) {

        clearMessages();
        if(!validateData())
            return;

        TravelCompany travelCompany = exchangeControlsToData();

        AdminService adminService = new AdminServiceImpl();
        adminService.createTravelCompany(travelCompany);
    }

    @FXML
    void onCancel(ActionEvent event) {
        parentController.clearLayOutField();
    }

    boolean validateData() {

        boolean hasInvalidMessage = false;

        if(nameField.getText().isEmpty()) {
            invalidNameMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(cityField.getText().isEmpty()) {
            invalidCityMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(countryField.getText().isEmpty()) {
            invalidCountryMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(addressField.getText().isEmpty()) {
            invalidAddressMessage.setVisible(true);
            hasInvalidMessage = true;
        }

        if(hasInvalidMessage)
            return false;

        return true;
    }

    void clearMessages() {
        invalidNameMessage.setVisible(false);
        invalidAddressMessage.setVisible(false);
        invalidCityMessage.setVisible(false);
        invalidCountryMessage.setVisible(false);

    }

    TravelCompany exchangeControlsToData() {
        TravelCompany travelCompany = new TravelCompany();
        travelCompany.setName(nameField.getText());
        travelCompany.setCountry(countryField.getText());
        travelCompany.setCity(cityField.getText());
        travelCompany.setAddress(addressField.getText());

        return travelCompany;
    }
}
