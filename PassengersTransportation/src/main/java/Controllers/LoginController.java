package Controllers;

import Controllers.Admin.AdminNavigationController;
import Controllers.TravelAgent.TravelAgentLandingController;
import Entities.User;
import Enums.UserTypes;
import Services.Classes.LoginServiceImpl;
import Services.LoginService;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    private Stage stage;

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button loginButton;

    @FXML
    private Text invalidText;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loginButton.setOnAction(this::OnLogin);
        invalidText.setVisible(false);
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    void OnLogin(javafx.event.ActionEvent event) {
        invalidText.setVisible(false);
        User user = new User();
        user.setName(usernameField.getText());
        user.setPassword(passwordField.getText());

        LoginService loginService = new LoginServiceImpl();
        if(!loginService.verifyLogin(user))
        {
            invalidText.setVisible(true);
            return;
        }


        UserTypes userType = UserTypes.fromInteger(user.getType());
        try {

            if(userType == UserTypes.Admin)
                loadAdminUI();

            if(userType == UserTypes.TravelAgent)
                loadTravelAgentUI();

        } catch(IOException ignored)
        {}
    }


    private void loadAdminUI() throws IOException {
        Stage stage = getStage();

        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/Admin/NavigationLanding.fxml"));
        Parent root = loader.load();

        double currentWidth = stage.getWidth();
        double currentHeight = stage.getHeight();

        stage.setScene(new Scene(root));
        stage.setTitle("Passenger Transportation : ADMIN");
        AdminNavigationController controller = loader.getController();
        controller.setStage(stage);

        stage.show();
    }

    private void loadTravelAgentUI() throws IOException {
        Stage stage = getStage();

        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/TravelAgent/TravelAgentLanding.fxml"));
        Parent root = loader.load();

        double currentWidth = stage.getWidth();
        double currentHeight = stage.getHeight();

        stage.setScene(new Scene(root));
        stage.setTitle("Passenger Transportation : TRAVEL AGENT");
        TravelAgentLandingController controller = loader.getController();
        controller.setStage(stage);

        stage.show();
    }


}
