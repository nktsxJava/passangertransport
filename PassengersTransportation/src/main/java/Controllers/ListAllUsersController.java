package Controllers;

import Controllers.Admin.AdminNavigationController;
import Controllers.TravelAgent.TravelAgentLandingController;
import Entities.User;
import Services.AdminService;
import Services.Classes.AdminServiceImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ListAllUsersController implements Initializable {

    private AdminNavigationController parentAdminController = new AdminNavigationController();

    @FXML
    private Button btnExit;

    @FXML
    private TableView<User> usersTable;

    @FXML
    private TableColumn<User, String> nameColumn;

    @FXML
    private TableColumn<User, String> emailColumn;

    @FXML
    private TableColumn<User, String> cityColumn;

    @FXML
    private TableColumn<User, String> countryColumn;


    public void setParentAdminController(AdminNavigationController parentAdminController) {
        this.parentAdminController = parentAdminController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        populateTableView();

        btnExit.setOnAction(this::onExit);
    }

    void onExit(ActionEvent event) {
        parentAdminController.clearLayOutField();
    }

    void populateTableView() {
        initTableView();

        AdminService adminService = new AdminServiceImpl();
        List<User> users = adminService.loadAllUsers();

        ObservableList<User> usersTableList = FXCollections.observableArrayList();
        usersTableList.addAll(users);

        usersTable.setItems(usersTableList);
    }

    void initTableView() {
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        cityColumn.setCellValueFactory(new PropertyValueFactory<>("city"));
        countryColumn.setCellValueFactory(new PropertyValueFactory<>("country"));
    }
}
