package utils;

import Entities.User;

public class UserSession {

    private static UserSession UserSession;
    private User loggedUser;

    private UserSession(User user) {
        this.loggedUser = user;
    }

    public static void createUserSession(User user) {
        if (UserSession == null) {
            UserSession = new UserSession(user);
        }
    }

    public static UserSession getUserSession() {
        return UserSession;
    }

    public static void clearUserSession() {
        UserSession = null;
    }

    public User getLoggedUser() {
        return this.loggedUser;
    }
}
