import Controllers.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class LaunchApp extends Application {

    public static void main (String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("view/Admin/AdminLanding.fxml"));
        Parent root = loader.load();
        stage.getIcons().add(new Image("images/bus.png"));
        stage.setTitle("Loginscreen");
        stage.setScene(new Scene(root));

        LoginController controller = loader.getController();
        controller.setStage(stage);

        stage.show();
    }
}
