package Repositories;

import Entities.DistributorTravelTickets;
import Entities.Travel;
import java.util.List;

public interface DistributorTravelTicketsRepository extends BaseTableRepository<Entities.DistributorTravelTickets> {
    public List<DistributorTravelTickets> findTravel(Travel travel);
}
