package Repositories;

import Entities.User;

import java.util.List;

public interface UserRepository extends BaseTableRepository<User>{
    public User findByUserName( String userName );
    public List<User> loadAllUsers();
}