package Repositories;

import Entities.LoggedHistory;

public interface LoggedHistoryRepository {
    public void saveHistory(LoggedHistory loggedUser );
}
