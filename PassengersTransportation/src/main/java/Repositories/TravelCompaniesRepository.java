package Repositories;

import Entities.TravelCompany;

import java.util.List;

public interface TravelCompaniesRepository extends BaseTableRepository<TravelCompany> {
    public List<TravelCompany> loadAllTravelCompanies();
}
