package Repositories.Classes;

import Entities.Distributor;
import Repositories.BaseTableRepository;
import Repositories.DistributorRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtil;

public class DistributorRepositoryImpl implements DistributorRepository {

    private static final Logger logger = LogManager.getLogger(DistributorRepositoryImpl.class);

    @Override
    public Distributor load(Integer Id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            Distributor distributor =  session.get(Distributor.class, Id);
            return distributor;

        } catch (Exception e) {
            logger.error("{}", e);
            return null;
        }
    }

    @Override
    public void create(Distributor distributor) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.save(distributor);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public Distributor update(Distributor distributor) {

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.beginTransaction();

            Distributor updatedDistributor = (Distributor)session.merge(distributor);

            transaction.commit();

            return updatedDistributor;

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            logger.error("{}", e);
            return null;
        }
    }

    @Override
    public void delete(Distributor distributor) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.delete(distributor);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public void makeTicketApplicationRequest() {

    }
}
