package Repositories.Classes;

import Entities.Order;
import Repositories.BaseTableRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtil;

public class OrderRepositoryImpl implements BaseTableRepository<Order> {

    private static final Logger logger = LogManager.getLogger(OrderRepositoryImpl.class);

    @Override
    public Order load(Integer Id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            Order order =  session.get(Order.class, Id);
            return order;

        } catch (Exception e) {
            logger.error("{}", e);
            return null;
        }
    }

    @Override
    public void create(Order order) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.save(order);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public Order update(Order order) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.getTransaction();
            transaction.begin();

            Order updatedOrder = (Order)session.merge(order);

            transaction.commit();
            return updatedOrder;

        } catch (Exception e) {
            logger.error("{}", e);
            if(transaction != null)
                transaction.rollback();


        }

        return null;
    }

    @Override
    public void delete(Order order) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.delete(order);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }
}
