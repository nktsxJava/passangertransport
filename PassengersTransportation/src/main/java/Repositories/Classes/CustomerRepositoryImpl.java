package Repositories.Classes;

import Entities.Customer;
import Repositories.BaseTableRepository;
import Repositories.CustomerRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtil;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerRepositoryImpl implements CustomerRepository {

    private static final Logger logger = LogManager.getLogger(CustomerRepositoryImpl.class);


    @Override
    public Customer load(Integer Id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            Customer customer =  session.get(Customer.class, Id);
            return customer;

        } catch (Exception e) {
            logger.error("{}", e);
            return null;
        }
    }

    @Override
    public void create(Customer customer) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.save(customer);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public Customer update(Customer customer) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.getTransaction();
            transaction.begin();

            Customer updatedCustomer = (Customer)session.merge(customer);

            transaction.commit();
            return updatedCustomer;

        } catch (Exception e) {
            logger.error("{}", e);
            if (transaction != null)
                transaction.rollback();
        }

        return null;
    }

    @Override
    public void delete(Customer customer) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.delete(customer);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    public boolean findCustomer(String name, String lastName,  String ucn)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();

        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Customer> query = builder.createQuery(Customer.class);
        Root<Customer> root = query.from(Customer.class);

        query.select(root);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(builder.equal(root.get("Ucn"), ucn));
        predicates.add(builder.equal(root.get("LastName"), lastName));
        predicates.add(builder.equal(root.get("Name"), name));

        query.where(predicates.toArray(new Predicate[]{}));

        List<Customer> results = session.createQuery(query).getResultList();
        if(results.size() <= 0)
            return false;

        return true;
    }
}
