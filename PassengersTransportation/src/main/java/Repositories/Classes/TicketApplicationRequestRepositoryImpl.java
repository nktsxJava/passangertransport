package Repositories.Classes;

import Entities.TicketApplicationRequest;
import Repositories.BaseTableRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtil;

public class TicketApplicationRequestRepositoryImpl implements BaseTableRepository<TicketApplicationRequest> {

    private static final Logger logger = LogManager.getLogger(TicketApplicationRequestRepositoryImpl.class);

    @Override
    public TicketApplicationRequest load(Integer Id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            TicketApplicationRequest ticketApplicationRequest =  session.get(TicketApplicationRequest.class, Id);
            return ticketApplicationRequest;

        } catch (Exception e) {

            logger.error("{}", e);
            return null;
        }
    }

    @Override
    public void create(TicketApplicationRequest ticketApplicationRequest) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.save(ticketApplicationRequest);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public TicketApplicationRequest update(TicketApplicationRequest ticketApplicationRequest) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.getTransaction();
            transaction.begin();

            TicketApplicationRequest updatedTicketApplicationRequest = (TicketApplicationRequest)session.merge(ticketApplicationRequest);

            transaction.commit();
            return updatedTicketApplicationRequest;

        } catch (Exception e) {
            logger.error("{}", e);
            if(transaction != null)
                transaction.rollback();


        }

        return null;
    }

    @Override
    public void delete(TicketApplicationRequest ticketApplicationRequest) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.delete(ticketApplicationRequest);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }
}
