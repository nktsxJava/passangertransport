package Repositories.Classes;

import Entities.DistributorTravelTickets;
import Entities.Travel;
import Repositories.DistributorTravelTicketsRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtil;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class DistributorTravelTicketsRepositoryImpl implements DistributorTravelTicketsRepository {

    private static final Logger logger = LogManager.getLogger(DistributorTravelTicketsRepositoryImpl.class);

    @Override
    public DistributorTravelTickets load(Integer Id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            DistributorTravelTickets distributorTravelTickets =  session.get(DistributorTravelTickets.class, Id);
            return distributorTravelTickets;

        } catch (Exception e) {

            logger.error("{}", e);
            return null;
        }

    }

    @Override
    public void create(DistributorTravelTickets distributorTravelTickets) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.save(distributorTravelTickets);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public DistributorTravelTickets update(DistributorTravelTickets distributorTravelTickets) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.beginTransaction();

            DistributorTravelTickets updatedDistributorTravelTickets = (DistributorTravelTickets)session.merge(distributorTravelTickets);

            transaction.commit();

            return updatedDistributorTravelTickets;

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            return null;
        }
    }

    @Override
    public void delete(DistributorTravelTickets updatedDistributorTravelTickets) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.delete(updatedDistributorTravelTickets);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public List<DistributorTravelTickets> findTravel(Travel travel) {

        Session session = HibernateUtil.getSessionFactory().openSession();

        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<DistributorTravelTickets> query = builder.createQuery(DistributorTravelTickets.class);
        Root<DistributorTravelTickets> root = query.from(DistributorTravelTickets.class);

        query.select(root);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(builder.equal(root.get("Travel"), travel));
        query.where(predicates.toArray(new Predicate[]{}));

        List<DistributorTravelTickets> results = session.createQuery(query).getResultList();
        return results;
    }
}
