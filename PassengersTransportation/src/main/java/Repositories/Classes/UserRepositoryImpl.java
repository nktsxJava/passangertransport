package Repositories.Classes;

import Entities.User;
import Repositories.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.List;


public class UserRepositoryImpl implements UserRepository {

    private static final Logger logger = LogManager.getLogger(UserRepositoryImpl.class);

    @Override
    public User load(Integer Id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            User user =  session.get(User.class, Id);
            return user;

        } catch (Exception e) {
            logger.error("{}", e);
            return null;
        }
    }

    @Override
    public void create(User user) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            //хеширане на паролата
            String hashedPass = user.getPassword();
            hashedPass = BCrypt.hashpw(hashedPass, BCrypt.gensalt());
            user.setPassword(hashedPass);

            session.save(user);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public User update(User user) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.beginTransaction();

            User mergedUser = (User) session.merge(user);

            transaction.commit();

            return mergedUser;

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
                logger.error("{}", e);
            }

            return null;
        }
    }

    @Override
    public void delete(User user) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.delete(user);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    public User findByUserName( String userName ) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        //Binding parameter for native query
        List<User> users = session.createNativeQuery("SELECT * FROM USERS where NAME = :username")
                .addEntity(User.class)
                .setParameter("username", userName)
                .getResultList();

        // Проверяваме дали е бил върнат някакъв роусет, ако не е връщаме null
        if (users.size() == 0)
            return null;

        // Ако е връщаме първият запис от лист-а, понеже очакваме да е само 1 с това име
        return users.get(0);
    }

    @Override
    public List<User> loadAllUsers() {
        Session session = HibernateUtil.getSessionFactory().openSession();

        //Binding parameter for native query
        List<User> users = session.createNativeQuery("SELECT * FROM USERS")
                .addEntity(User.class)
                .getResultList();

        if (users.size() == 0)
            return null;

        return users;
    }
}