package Repositories.Classes;

import Entities.DistributorTravelTickets;
import Entities.TravelCompany;
import Repositories.TravelCompaniesRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtil;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class TravelCompanyRepositoryImpl implements TravelCompaniesRepository {

    private static final Logger logger = LogManager.getLogger(TravelCompanyRepositoryImpl.class);

    @Override
    public TravelCompany load(Integer Id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            TravelCompany travelCompany =  session.get(TravelCompany.class, Id);
            return travelCompany;

        } catch (Exception e) {
            logger.error("{}", e);
            return null;
        }
    }

    @Override
    public void create(TravelCompany travelCompany) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.save(travelCompany);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public TravelCompany update(TravelCompany travelCompany) {

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.getTransaction();
            transaction.begin();

            TravelCompany updatedTravelCompany = (TravelCompany)session.merge(travelCompany);

            transaction.commit();
            return updatedTravelCompany;

        } catch (Exception e) {
            if(transaction != null)
            {
                logger.error("{}", e);
                transaction.rollback();
            }

            return null;
        }
    }

    @Override
    public void delete(TravelCompany travelCompany) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.delete(travelCompany);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public List<TravelCompany> loadAllTravelCompanies() {

        Session session = HibernateUtil.getSessionFactory().openSession();

        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<TravelCompany> query = builder.createQuery(TravelCompany.class);
        Root<TravelCompany> root = query.from(TravelCompany.class);

        query.select(root);
        List<TravelCompany> results = session.createQuery(query).getResultList();

        return results;
    }

}
