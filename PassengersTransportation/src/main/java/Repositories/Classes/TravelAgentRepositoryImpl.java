package Repositories.Classes;

import Entities.Travel;
import Entities.TravelAgent;
import Repositories.BaseTableRepository;
import Repositories.TravelAgentRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtil;

public class TravelAgentRepositoryImpl implements TravelAgentRepository {

    private static final Logger logger = LogManager.getLogger(TravelAgentRepositoryImpl.class);

    @Override
    public TravelAgent load(Integer Id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            TravelAgent travelAgent =  session.get(TravelAgent.class, Id);
            return travelAgent;

        } catch (Exception e) {
            logger.error("{}", e);
            return null;
        }
    }

    @Override
    public void create(TravelAgent travelAgent) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.save(travelAgent);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public TravelAgent  update(TravelAgent travelAgent) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.getTransaction();
            transaction.begin();

            TravelAgent updatedTravelAgent = (TravelAgent)session.merge(travelAgent);

            transaction.commit();
            return updatedTravelAgent;

        } catch (Exception e) {
            logger.error("{}", e);
            if(transaction != null)
                transaction.rollback();
        }

        return null;
    }

    @Override
    public void delete(TravelAgent travelAgent) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.delete(travelAgent);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public void createTravel(Travel travel) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.save(travel);

        } catch (Exception e) {
            logger.error("{}", e);
        }

    }
}
