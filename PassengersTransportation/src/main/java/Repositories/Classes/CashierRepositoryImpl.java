package Repositories.Classes;

import Entities.Cashier;
import Repositories.BaseTableRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtil;

public class CashierRepositoryImpl implements BaseTableRepository<Cashier> {

    private static final Logger logger = LogManager.getLogger(CashierRepositoryImpl.class);

    @Override
    public Cashier load(Integer Id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            Cashier cashier =  session.get(Cashier.class, Id);
            return cashier;

        } catch (Exception e) {
            logger.error("{}", e);
            return null;
        }
    }

    @Override
    public void create(Cashier cashier) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.save(cashier);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public Cashier update(Cashier cashier) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.getTransaction();
            transaction.begin();

            Cashier updatedCashier = (Cashier)session.merge(cashier);

            transaction.commit();
            return updatedCashier;

        } catch (Exception e) {
            logger.error("{}", e);
            if (transaction != null)
                transaction.rollback();
        }

        return null;
    }

    @Override
    public void delete(Cashier cashier) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.delete(cashier);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }
}
