package Repositories.Classes;

import Entities.Ticket;
import Repositories.BaseTableRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtil;

public class TicketRepositoryImpl implements BaseTableRepository<Ticket> {

    private static final Logger logger = LogManager.getLogger(TicketRepositoryImpl.class);

    @Override
    public Ticket load(Integer Id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            Ticket ticket =  session.get(Ticket.class, Id);
            return ticket;

        } catch (Exception e) {

            logger.error("{}", e);
            return null;
        }
    }

    @Override
    public void create(Ticket ticket) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.save(ticket);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }

    @Override
    public Ticket update(Ticket ticket) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.getTransaction();
            transaction.begin();

            Ticket updatedTicket = (Ticket)session.merge(ticket);

            transaction.commit();
            return updatedTicket;

        } catch (Exception e) {
            logger.error("{}", e);
            if(transaction != null)
                transaction.rollback();


        }

        return null;
    }

    @Override
    public void delete(Ticket ticket) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.delete(ticket);

        } catch (Exception e) {
            logger.error("{}", e);
        }
    }
}
