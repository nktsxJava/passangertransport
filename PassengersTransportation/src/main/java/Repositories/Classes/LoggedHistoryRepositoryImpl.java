package Repositories.Classes;

import Repositories.LoggedHistoryRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import utils.HibernateUtil;
import Entities.LoggedHistory;

public class LoggedHistoryRepositoryImpl implements LoggedHistoryRepository {

    private static final Logger logger = LogManager.getLogger(LoggedHistoryRepositoryImpl.class);

    public LoggedHistoryRepositoryImpl() {}

    @Override
    public void saveHistory(LoggedHistory loggedHistory) {

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            session.save(loggedHistory);

        } catch (Exception e) {

            if (transaction != null) {
                logger.error("{}", e);
                transaction.rollback();
            }
        }
    }
}
