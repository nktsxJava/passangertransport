package Repositories;

public interface BaseTableRepository<TYPE> {
    public TYPE load(Integer Id);
    public void create(TYPE record);
    public TYPE update(TYPE record);
    public void delete(TYPE record);
}
