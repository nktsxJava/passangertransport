package Repositories;

import Entities.Distributor;

public interface DistributorRepository extends BaseTableRepository<Distributor>{
    public void makeTicketApplicationRequest();
}
