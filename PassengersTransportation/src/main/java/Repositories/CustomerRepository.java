package Repositories;

import Entities.Customer;

public interface CustomerRepository extends BaseTableRepository<Customer> {
    public boolean findCustomer(String name, String lastName,  String ucn);
}
