package Repositories;

import Entities.Travel;
import Entities.TravelAgent;

public interface TravelAgentRepository extends BaseTableRepository<TravelAgent>{
    public void createTravel(Travel travel);
}
