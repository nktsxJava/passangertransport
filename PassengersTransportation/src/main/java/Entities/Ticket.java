package Entities;

import javax.persistence.*;

@Entity
@Table(name = "TICKETS")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer Id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAVEL_ID", nullable = false)
    private Travel Travel;

    @Column(name = "TYPE")
    private Integer Type;

    @Column(name = "PRICE")
    private Integer Price;

    @Column(name = "SEAT_NUMBER")
    private short SeatNum;


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Travel getTravel() {
        return Travel;
    }

    public void setTravel(Travel travel) {
        Travel = travel;
    }

    public Integer getType() {
        return Type;
    }

    public void setType(Integer type) {
        Type = type;
    }

    public Integer getPrice() {
        return Price;
    }

    public void setPrice(Integer price) {
        Price = price;
    }

    public short getSeatNum() {
        return SeatNum;
    }

    public void setSeatNum(short seatNum) {
        SeatNum = seatNum;
    }
}
