package Entities;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TRAVELS")
public class Travel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer Id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAVEL_AGENT_ID", nullable = false)
    private TravelAgent TravelAgent;

    @Column(name = "TYPE")
    private Integer Type;

    @Column(name = "DESTINATION")
    private String Destination;

    @Column(name = "DEPARTURE")
    private LocalDate Departure;

    @Column(name = "ARRIVAL")
    private LocalDate Arrival;

    @Column(name = "SEATS_COUT")
    private Integer SeatsCount;

    @Column(name = "TRANSPORT_TYPE")
    private Integer TransportType;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Entities.TravelAgent getTravelAgent() {
        return TravelAgent;
    }

    public void setTravelAgent(Entities.TravelAgent travelAgent) {
        TravelAgent = travelAgent;
    }

    public Integer getType() {
        return Type;
    }

    public void setType(Integer type) {
        Type = type;
    }

    public String getDestination() {
        return Destination;
    }

    public void setDestination(String destination) {
        Destination = destination;
    }

    public LocalDate getDeparture() {
        return Departure;
    }

    public void setDeparture(LocalDate departure) {
        Departure = departure;
    }

    public Integer getSeatsCount() {
        return SeatsCount;
    }

    public void setSeatsCount(Integer seatsCount) {
        SeatsCount = seatsCount;
    }

    public Integer getTransportType() {
        return TransportType;
    }

    public void setTransportType(Integer transportType) {
        TransportType = transportType;
    }

    public LocalDate getArrival() {
        return Arrival;
    }

    public void setArrival(LocalDate arrival) {
        Arrival = arrival;
    }
}
