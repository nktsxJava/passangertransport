package Entities;

import Enums.UserTypes;

import javax.persistence.*;

@Entity
@Table(name = "USERS")
public class User {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer Id;

    @Column(name = "TYPE")
    private Integer Type;

    @Column(name = "NAME")
    private String Name;

    @Column(name = "PASSWORD")
    private String Password;

    @Column(name = "COUNTRY")
    private String Country;

    @Column(name = "CITY")
    private String City;

    @Column(name = "ADDRESS")
    private String Address;

    @Column(name = "EMAIL")
    private String Email;

    @OneToOne(mappedBy = "User", cascade = CascadeType.ALL, orphanRemoval = true)
    private Distributor Distributor;

    @OneToOne(mappedBy = "User", cascade = CascadeType.ALL, orphanRemoval = true)
    private TravelAgent TravelAgent;

    @OneToOne(mappedBy = "User", cascade = CascadeType.ALL, orphanRemoval = true)
    private Cashier Cashier;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public Integer getType() {
        return Type;
    }

    public void setType(Integer type) {
        Type = type;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

   private Distributor getDistributor() {
       return Distributor;
   }

   private void setDistributor(Distributor distributor) {
       this.Distributor = distributor;
   }

   private TravelAgent getTravelAgent() {
       return TravelAgent;
   }

   private void setTravelAgent(TravelAgent travelAgent) {
       this.TravelAgent = travelAgent;
   }

   private Cashier getCashier() {
       return Cashier;
   }

   private void setCashier(Cashier cashier) {
       this.Cashier = cashier;
   }

   public <TYPE> TYPE getRole() {

       Integer userType = this.getType();
       UserTypes eUserType = UserTypes.fromInteger(userType);

       switch (eUserType) {
           case TravelAgent:
               return (TYPE) this.getTravelAgent();
           case Distributor:
               return (TYPE) this.getDistributor();
           case Cashier:
               return (TYPE) this.getCashier();
       }

       return null;
   }
}
