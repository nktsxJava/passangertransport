package Entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ORDERS")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer Id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKET_ID", nullable = false)
    private Ticket Ticket;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID", nullable = false)
    private Customer Customer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CASHIER_ID", nullable = false)
    private Cashier Cashier;

    @Column(name = "DATE")
    private Date Date;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Customer getCustomer() {
        return Customer;
    }

    public void setCustomer(Customer customer) {
        Customer = customer;
    }

    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date date) {
        Date = date;
    }

    public Entities.Ticket getTicket() {
        return Ticket;
    }

    public void setTicket(Entities.Ticket ticket) {
        Ticket = ticket;
    }

    public Entities.Cashier getCashier() {
        return Cashier;
    }

    public void setCashier(Entities.Cashier cashier) {
        Cashier = cashier;
    }
}
