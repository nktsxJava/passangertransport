package Entities;

import javax.persistence.*;

@Entity
@Table(name = "CUSTOMERS")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer Id;

    @Column(name = "FIRST_NAME")
    private String Name;

    @Column(name = "LAST_NAME")
    private String LastName;

    @Column(name = "UCN")
    private String Ucn;

    @Column(name = "AGE")
    private Integer Age;

    @Column(name = "ADDRESS")
    private String Address;

    @Column(name = "PHONE_NUMBER")
    private String PhoneNumber;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public Integer getAge() {
        return Age;
    }

    public void setAge(Integer age) {
        Age = age;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getUcn() {
        return Ucn;
    }

    public void setUcn(String ucn) {
        Ucn = ucn;
    }
}
