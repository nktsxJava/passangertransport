package Entities;

import javax.persistence.*;

@Entity
@Table(name = "TICKETS_APPLICATION_REQUESTS")
public class TicketApplicationRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer Id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DISTRIBUTOR_ID", nullable = false)
    private Distributor distributor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAVEL_AGENT_ID", nullable = false)
    private TravelAgent travelAgent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAVEL_ID", nullable = false)
    private Travel Travel;

    @Column(name = "TICKETS_COUNT")
    private Integer TicketsCount;

    @Column(name = "REQUEST_STATUS")
    private Integer Status;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Entities.Distributor getDistributor() {
        return distributor;
    }

    public void setDistributor(Entities.Distributor distributor) {
        this.distributor = distributor;
    }

    public Entities.Travel getTravel() {
        return Travel;
    }

    public void setTravel(Entities.Travel travel) {
        Travel = travel;
    }

    public Integer getTicketsCount() {
        return TicketsCount;
    }

    public void setTicketsCount(Integer ticketsCount) {
        TicketsCount = ticketsCount;
    }

    public TravelAgent getTravelAgent() {
        return travelAgent;
    }

    public void setTravelAgent(TravelAgent travelAgent) {
        this.travelAgent = travelAgent;
    }

    public Integer getStatus() {
        return Status;
    }

    public void setStatus(Integer status) {
        Status = status;
    }
}
