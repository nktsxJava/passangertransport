package Entities;

import javax.persistence.*;

@Entity
@Table(name = "CASHIERS")
public class Cashier {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer Id;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID", nullable = false)
    private User User;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DISTRIBUTOR_ID", nullable = false)
    private Distributor Distributor;


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public User getUser() {
        return User;
    }

    public void setUser(User user) {
        User = user;
    }

    public Distributor getDistributor() {
        return Distributor;
    }

    public void setDistributor(Distributor distributor) {
        Distributor = distributor;
    }
}
