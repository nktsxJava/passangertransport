package Entities;

import javax.persistence.*;

@Entity
@Table(name = "TRAVEL_COMPANIES")
public class TravelCompany {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer Id;

    @Column(name = "NAME")
    private String Name;

    @Column(name = "ADDRESS")
    private String Address;

    @Column(name = "CITY")
    private String City;

    @Column(name = "COUNTRY")
    private String Country;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    @Override
    public String toString() {
        return this.Name;
    }
}
