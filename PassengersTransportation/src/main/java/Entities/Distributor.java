package Entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "DISTRIBUTORS")
public class Distributor {

    public Distributor() {
        this.Id = null;
        this.User = null;
        this.TravelAgent = null;
    }

    public Distributor(Integer Id, User user, TravelAgent travelAgent) {
        this.Id = Id;
        this.User = user;
        this.TravelAgent = travelAgent;
    }

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer Id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", nullable = false)
    private User User;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAVEL_AGENT_ID", nullable = false)
    private TravelAgent TravelAgent;

    @OneToMany(mappedBy = "Distributor", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DistributorTravelTickets> travelTicketsList = new ArrayList<>();

    @OneToMany(mappedBy = "Distributor", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Cashier> cashiers = new ArrayList<>();

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public User getUser() {
        return User;
    }

    public void setUser(User user) {
        User = user;
    }

    public TravelAgent getTravelAgent() {
        return TravelAgent;
    }

    public void setTravelAgent(TravelAgent travelAgent) {
        TravelAgent = travelAgent;
    }

    public List<DistributorTravelTickets> getTravelTicketsList() {
        return travelTicketsList;
    }

    public void setTravelTicketsList(List<DistributorTravelTickets> travelTicketsList) {
        this.travelTicketsList = travelTicketsList;
    }

    public List<Cashier> getCashiers() {
        return cashiers;
    }

    public void setCashiers(List<Cashier> cashiers) {
        this.cashiers = cashiers;
    }
}
