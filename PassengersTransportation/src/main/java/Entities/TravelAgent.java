package Entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TRAVEL_AGENTS")
public class TravelAgent {

    public TravelAgent() {
    }

    public TravelAgent(Integer Id, User user, TravelCompany travelCompany) {
        this.Id = Id;
        this.User = user;
        this.TravelCompany = travelCompany;
    }

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer Id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", nullable = false)
    private User User;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAVEL_COMPANY_ID", nullable = false)
    private TravelCompany TravelCompany;

    @OneToMany(mappedBy = "travelAgent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TicketApplicationRequest> ticketApplicationRequest = new ArrayList<>();

    @OneToMany(mappedBy = "TravelAgent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Distributor> distributors = new ArrayList<>();

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public User getUser() {
        return User;
    }

    public void setUser(User user) {
        User = user;
    }

    public TravelCompany getTravelCompany() {
        return TravelCompany;
    }

    public void setTravelCompany(TravelCompany travelCompany) {
        TravelCompany = travelCompany;
    }

    public List<TicketApplicationRequest> getTicketApplicationRequest() {
        return ticketApplicationRequest;
    }

    public void setTicketApplicationRequest(List<TicketApplicationRequest> ticketApplicationRequest) {
        this.ticketApplicationRequest = ticketApplicationRequest;
    }
}
