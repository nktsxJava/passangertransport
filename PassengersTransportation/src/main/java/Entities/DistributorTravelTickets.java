package Entities;

import javax.persistence.*;

@Entity
@Table(name = "DISTRIBUTORS_TRAVEL_TICKETS")
public class DistributorTravelTickets {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer Id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DISTRIBUTOR_ID", nullable = false)
    private Distributor Distributor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAVEL_ID", nullable = false)
    private Travel Travel;

    @Column(name = "TICKETS_COUNT")
    private Integer TicketsCount;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Entities.Distributor getDistributor() {
        return Distributor;
    }

    public void setDistributor(Entities.Distributor distributor) {
        Distributor = distributor;
    }

    public Entities.Travel getTravel() {
        return Travel;
    }

    public void setTravel(Entities.Travel travel) {
        Travel = travel;
    }

    public Integer getTicketsCount() {
        return TicketsCount;
    }

    public void setTicketsCount(Integer ticketsCount) {
        TicketsCount = ticketsCount;
    }
}
