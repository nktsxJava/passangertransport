package Entities;

import Enums.LoginStatuses;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "LOGGED_HISTORY")
public class LoggedHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", nullable = false)
    private User user;

    @Column(name = "STATUS")
    private LoginStatuses status;

    @Column(name = "LOG_IN_TIME")
    private Timestamp logInTime;

    @Column(name = "LOG_OUT_TIME")
    private Timestamp logOutTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LoginStatuses getStatus() {
        return status;
    }

    public void setStatus(LoginStatuses status) {
        this.status = status;
    }

    public Timestamp getLogInTime() {
        return logInTime;
    }

    public void setLogInTime(Timestamp logInTime) {
        this.logInTime = logInTime;
    }

    public Timestamp getLogOutTime() {
        return logOutTime;
    }

    public void setLogOutTime(Timestamp logOutTime) {
        this.logOutTime = logOutTime;
    }
}


